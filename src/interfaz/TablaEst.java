/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package interfaz;

/**
 *
 * @author ian
 */
public class TablaEst extends javax.swing.table.AbstractTableModel {

	TablaFaenas tfaeLocal;
	String[] colNames = new String[] {"Region","Numero de faenas","%"};
	
	
	public TablaEst(TablaFaenas faenas) {
		this.tfaeLocal = faenas;
	}

	@Override
	public String getColumnName (int col) {
		return colNames[col];
	}
	@Override
	public int getRowCount() {
		return Lugar.regiones.length;
	}

	@Override
	public int getColumnCount() {
		return colNames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		switch (columnIndex) {
			case 0:
				return Lugar.regiones[rowIndex];
			
			case 1:
				if (rowIndex<tfaeLocal.nFaeRegion.length)
					return tfaeLocal.nFaeRegion[rowIndex];
				return -1;
			
			case 2:
				float perc = (float) (tfaeLocal.nFaeRegion[rowIndex]*100.0/tfaeLocal.totalFaenas);
				return Math.round(perc*100.0)/100.0;
			
			default:
				return "";
		}
	}
	
}
