/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package interfaz;

/**
 *
 * @author ian
 */
public class Lugar {
	
	public static final String[] regiones;
	public static final String[][] comunas;
	
	static{
		regiones = new String[]{
			"Arica y Parinacota",
			"Tarapaca",
			"Antofagasta",
			"Atacama",
			"Coquimbo",
			"Valparaiso",
			"Metropolitana de Santiago",
			"Libertador Bernardo Ohiggins",
			"Maule",
			"Biobio",
			"Araucania",
			"Los Rios",
			"Los Lagos",
			"Aysen del General Carlos Ibanez del Campo",
			"Magallanes y de la Antartica Chilena"
			
		};

		comunas = new String[15][];
		
		comunas[0] = new String[]{ //region 15
			"Arica",
			"Camarones",
			"General Lagos",
			"Putre"
		};

		comunas[1] = new String[]{ //region 1
			"Alto Hospicio",
			"Iquique",
			"Camiña",
			"Colchane",
			"Huara",
			"Pica",
			"Pozo Almonte"
		};

		comunas[2] = new String[]{ //region 2
			"Antofagasta",
			"Mejillones",
			"Sierra Gorda",
			"Taltal",
			"Calama",
			"Ollague",
			"San Pedro de Atacama",
			"Maria Elena",
			"Tocopilla"
		};

		comunas[3] = new String[]{ //region 3
			"Chanaral",
			"Diego de Almagro",
			"Caldera",
			"Copiapo",
			"Tierra Amarilla",
			"Alto del Carmen",
			"Freirina",
			"Huasco",
			"Vallenar"
		};

		comunas[4] = new String[]{ //region 4
			"Canela",
			"Illapel",
			"Los Vilos",
			"Salamanca",
			"Andacollo",
			"Coquimbo",
			"La Higuera",
			"La Serena",
			"Paihuano",
			"Vicuna",
			"Combarbala",
			"Monte Patria",
			"Ovalle",
			"Punitaqui",
			"Rio Hurtado"
		};

		comunas[5] = new String[]{ //region 5
			"Isla de Pascua",
			"Calle Larga",
			"Los Andes",
			"Rinconada",
			"San Esteban",
			"Cabildo",
			"La Ligua",
			"Papudo",
			"Petorca",
			"Zapallar",
			"Hijuelas",
			"La Calera",
			"La Cruz",
			"Nogales",
			"Quillota",
			"Algarrobo",
			"Cartagena",
			"El Quisco",
			"El Tabo",
			"San Antonio",
			"Santo Domingo",
			"Catemu",
			"Llayllay",
			"Panquehue",
			"Putaendo",
			"San Felipe",
			"Santa Maria",
			"Casablanca",
			"Concón",
			"Juan Fernández",
			"Puchuncavi",
			"Quintero",
			"Valparaiso",
			"Vina del Mar",
			"Limache",
			"Olmue",
			"Quilpue",
			"Villa Alemana"
		};

		comunas[6] = new String[]{ //region RM
			"Colina",
			"Lampa",
			"Tiltil",
			"Pirque",
			"Puente Alto",
			"San Jose de Maipo",
			"Buin",
			"Calera de Tango",
			"Paine",
			"San Bernardo",
			"Alhue",
			"Curacavi",
			"Maria Pinto",
			"Melipilla",
			"San Pedro",
			"Cerrillos",
			"Cerro Navia",
			"Conchali",
			"El Bosque",
			"Estacion Central",
			"Huechuraba",
			"Independencia",
			"La Cisterna",
			"La Granja",
			"La Florida",
			"La Pintana",
			"La Reina",
			"Las Condes",
			"Lo Barnechea",
			"Lo Espejo",
			"Lo Prado",
			"Macul",
			"Maipu",
			"Ñunoa",
			"Pedro Aguirre Cerda",
			"Penalolen",
			"Providencia",
			"Pudahuel",
			"Quilicura",
			"Quinta Normal",
			"Recoleta",
			"Renca",
			"San Miguel",
			"San Joaquin",
			"San Ramon",
			"Santiago",
			"Vitacura",
			"El Monte",
			"Isla de Maipo",
			"Padre Hurtado",
			"Penaflor",
			"Talagante"
		};

		comunas[7] = new String[]{ //region 6
			"Codegua",
			"Coinco",
			"Coltauco",
			"Donihue",
			"Graneros",
			"Las Cabras",
			"Machali",
			"Malloa",
			"Mostazal",
			"Olivar",
			"Peumo",
			"Pichidegua",
			"Quinta de Tilcoco",
			"Rancagua",
			"Rengo",
			"Requinoa",
			"San Vicente de Tagua Tagua",
			"La Estrella",
			"Litueche",
			"Marchihue",
			"Navidad",
			"Paredones",
			"Pichilemu",
			"Chepica",
			"Chimbarongo",
			"Lolol",
			"Nancagua",
			"Palmilla",
			"Peralillo",
			"Placilla",
			"Pumanque",
			"San Fernando",
			"Santa Cruz"
		};

		comunas[8] = new String[]{ //region 7
			"Cauquenes",
			"Chanco",
			"Pelluhue",
			"Curico",
			"Hualañe",
			"Licanten",
			"Molina",
			"Rauco",
			"Romeral",
			"Sagrada Familia",
			"Teno",
			"Vichuquen",
			"Colbun",
			"Linares",
			"Longavi",
			"Parral",
			"Retiro",
			"San Javier",
			"Villa Alegre",
			"Yerbas Buenas",
			"Constitucion",
			"Curepto",
			"Empedrado",
			"Maule",
			"Pelarco",
			"Pencahue",
			"Rio Claro",
			"San Clemente",
			"San Rafael",
			"Talca"
		};

		comunas[9] = new String[]{ //region 8
			"Arauco",
			"Canete",
			"Contulmo",
			"Curanilahue",
			"Lebu",
			"Los Alamos",
			"Tirua",
			"Alto Biobio",
			"Antuco",
			"Cabrero",
			"Laja",
			"Los Angeles",
			"Mulchen",
			"Nacimiento",
			"Negrete",
			"Quilaco",
			"Quilleco",
			"San Rosendo",
			"Santa Barbara",
			"Tucapel",
			"Yumbel",
			"Chiguayante",
			"Concepcion",
			"Coronel",
			"Florida",
			"Hualpen",
			"Hualqui",
			"Lota",
			"Penco",
			"San Pedro de la Paz",
			"Santa Juana",
			"Talcahuano",
			"Tome",
			"Bulnes",
			"Chillan",
			"Chillan Viejo",
			"Cobquecura",
			"Coelemu",
			"Coihueco",
			"El Carmen",
			"Ninhue",
			"Niquen",
			"Pemuco",
			"Pinto",
			"Portezuelo",
			"Quillon",
			"Quirihue",
			"Ranquil",
			"San Carlos",
			"San Fabian",
			"San Ignacio",
			"San Nicolas",
			"Treguaco",
			"Yungay"
		};

		comunas[10] = new String[]{ //region 9
			"Carahue",
			"Cholchol",
			"Cunco",
			"Curarrehue",
			"Freire",
			"Galvarino",
			"Gorbea",
			"Lautaro",
			"Loncoche",
			"Melipeuco",
			"Nueva Imperial",
			"Padre Las Casas",
			"Perquenco",
			"Pitrufquen",
			"Pucon",
			"Puerto Saavedra",
			"Temuco",
			"Teodoro Schmidt",
			"Tolten",
			"Vilcun",
			"Villarrica",
			"Angol",
			"Collipulli",
			"Curacautin",
			"Ercilla",
			"Lonquimay",
			"Los Sauces",
			"Lumaco",
			"Puren",
			"Renaico",
			"Traiguen",
			"Victoria"
		};

		comunas[11] = new String[]{ //region 14
			"Corral",
			"Lanco",
			"Los Lagos",
			"Mafil",
			"Mariquina",
			"Paillaco",
			"Panguipulli",
			"Valdivia"
		};

		comunas[12] = new String[]{ //region 10
			"Ancud",
			"Castro",
			"Chonchi",
			"Curaco de Velez",
			"Dalcahue",
			"Puqueldon",
			"Queilen",
			"Quemchi",
			"Quellon",
			"Quinchao",
			"Calbuco",
			"Cochamo",
			"Fresia",
			"Frutillar",
			"Llanquihue",
			"Los Muermos",
			"Maullin",
			"Puerto Montt",
			"Puerto Varas",
			"Osorno",
			"Puerto Octay",
			"Purranque",
			"Puyehue",
			"Rio Negro",
			"San Juan de la Costa",
			"San Pablo",
			"Chaiten",
			"Futaleufu",
			"Hualaihue",
			"Palena"
		};

		comunas[13] = new String[]{ //region 11
			"Aysen",
			"Cisnes",
			"Guaitecas",
			"Cochrane",
			"OHiggins",
			"Tortel",
			"Coyhaique",
			"Lago Verde",
			"Chile Chico",
			"Rio Ibanez"
		};

		comunas[14] = new String[]{ //region 12
			"Antartica",
			"Cabo de Hornos",
			"Laguna Blanca",
			"Punta Arenas",
			"Rio Verde",
			"San Gregorio",
			"Porvenir",
			"Primavera",
			"Timaukel",
			"Natales",
			"Torres del Paine"
		};
	};
	
	
	/**
	 * Busca la comuna en la lista de comunas y nos devuelve el nombre
	 * de la region en donde se encuentra la comuna.
	 * @param comuna Es la comuna que se busca.
	 * @return El nombre de la region
	 */
	public static String encontrarComuna (String comuna) {
		String region="";
		
		for (int i=0; i< comunas.length; i++) {
			
			if (TablaFaenas.find(comuna, comunas[i])!=-1) {
				region = regiones[i];
			}
		}
		
		return region;
	}
}
