/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package interfaz;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author ian
 */
public class TablaFaenas {
	
	private java.util.ArrayList[][] faenas;
	public int totalFaenas;
	public int[] nFaeRegion;
	
	public TablaFaenas () {
		 
		 
		faenas = new ArrayList[Lugar.comunas.length][];
		totalFaenas = 0;
		nFaeRegion = new int[Lugar.regiones.length];

		for (int i=0; i<faenas.length; i++) {
			faenas[i] = new ArrayList[Lugar.comunas[i].length];
		}
		
	}
	
	public boolean ingresar(Faena faena) {
		int nReg, nCol;
		String regCor;
		
		nReg = find(faena.getRegion(), Lugar.regiones);
		
		if (nReg!=-1)
			nCol = find(faena.getComuna(), Lugar.comunas[nReg]);
		else {/*
			System.out.println("\nFaena: "+faena.getNombre());
			System.out.println("**Region: "+faena.getRegion());
			System.out.println("Comuna: "+faena.getComuna());*/
			return false;
		}
		
		if (nCol==-1) {/*
			System.out.println("\nFaena: "+faena.getNombre());
			System.out.println("Region: "+faena.getRegion());
			System.out.println("**Comuna: "+faena.getComuna());*/
			regCor = Lugar.encontrarComuna(faena.getComuna());
			faena.setRegion(regCor);
			//System.out.println("r correcta: "+regCor);
			if ((nReg = find(regCor, Lugar.regiones))== -1)
					return false;
			nCol = find(faena.getComuna(), Lugar.comunas[nReg]);
		}
		
		if (faenas[nReg][nCol]==null) {
			faenas[nReg][nCol] =  new ArrayList(0);
		}
		
		faenas[nReg][nCol].add(faena);
		totalFaenas++;
		nFaeRegion[nReg]++;
		
		return true;
	}
	
	
	public ArrayList<Faena> getLista (int region, int comuna) {
		
		try {
			if (faenas[region][comuna]==null)
				return new ArrayList<Faena>(0);
			return faenas[region][comuna];
		}catch (IndexOutOfBoundsException er) {
			return new ArrayList<Faena>(0);
		}
	}
	
	
	public ArrayList<Faena> getLista (int region) {
		
		ArrayList list = new ArrayList(0);

		for (int i=0; i< faenas[region].length; i++) {

			if (faenas[region][i]!=null) {
				
				for (int j=0; j< faenas[region][i].size(); j++) {
					list.add(faenas[region][i].get(j));
				}

			}
		}
		return list;
	}
	
	public boolean importarDatos(java.io.File archivo){
        java.io.FileReader arch;
		java.io.BufferedReader buf;
		 
		String nombre, region, comuna, tipo, comentario;
        
		try{
			arch = new java.io.FileReader(archivo);
			buf = new java.io.BufferedReader(arch);
			
            
			while ((nombre = buf.readLine())!=null) {
				region = buf.readLine();
				comuna = buf.readLine();
				tipo = buf.readLine();
				comentario = buf.readLine();
				
				ingresar(new Faena(nombre, region, comuna, tipo, comentario));
			}

			buf.close();
			arch.close();

			return true;
		}catch(java.io.FileNotFoundException e1){return false;}
		catch(java.io.IOException e2){return true;}
	 }
	 
	/**
	 * Busca la posicion de cierto string en una lista de string. Indica el
	 * index, partiento de 0 como primera posicion, en donde se encuentra el
	 * string buscado.
	 * @param buscado Es el string que se busca
	 * @param list Es la lista de string en la que se quiere buscar
	 * @return El numero de posicion, o -1 si no se encuentra
	 */
	 public static int find (String buscado, String[] list) {
		 
		 for (int i=0; i<list.length; i++) {
			 if (list[i].compareToIgnoreCase(buscado)==0) {
				 return i;
			 }
		 }
		 return -1;
	 }
	 
	 public void guardarBase() throws IOException {
		 
		 java.io.ObjectOutputStream reg= new java.io.ObjectOutputStream(
				new java.io.FileOutputStream("base.data"));
		 
		 for (int i=0; i< Lugar.comunas.length; i++) {
			
			for (int j=0; j< Lugar.comunas[i].length; j++) {
				if (faenas[i][j] != null) {
					for (int k=0; k< faenas[i][j].size(); k++) {
						reg.writeObject(faenas[i][j].get(k));
					}
				}
			}
		 }
		 
		 reg.close();
	 }
	 
	 public boolean cargarBase() {
     
		 java.io.ObjectInputStream load;
		 
		 try{

			// CARGA DE REGISTRO DE ANIMALES EN VENTA
            load= new java.io.ObjectInputStream(
                    new java.io.FileInputStream("base.data"));
            Faena nueva;
			
			while ((nueva=(Faena)load.readObject())!=null) {
				ingresar(nueva);
			}
			
			load.close();
			
            return true;
        }catch (java.io.FileNotFoundException e1){return false;}
        catch (java.io.IOException e2){return true;}
        catch (java.lang.ClassNotFoundException e3){return false;}
        
	 }

	
}
