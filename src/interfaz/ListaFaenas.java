/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ian
 */
public class ListaFaenas extends AbstractTableModel{
    private final String[] nombreCols={"Nombre","Region","Comuna","Tipo","Comentario"};
    
    TablaFaenas faenas;
	private ArrayList<Faena> listaActual;
    
    public ListaFaenas(TablaFaenas faenas){
        super();
        this.faenas = faenas;
		//listaActual = new ArrayList<Faena>(0);
		listaActual = this.faenas.getLista(0);
    }
	
    public void setlista(int region, int comuna){
		if (comuna==-1)
			listaActual = faenas.getLista(region);
		else
			listaActual = faenas.getLista(region, comuna);
    }
	
    @Override
    public String getColumnName(int col){
        return nombreCols[col];
    }
    @Override
    public int getRowCount() {
        return listaActual.size();
    }

    @Override
    public int getColumnCount() {
        return nombreCols.length;
    }

    @Override
    @SuppressWarnings("empty-statement")
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (listaActual.isEmpty()) {
            return null;
        }
        
        switch(columnIndex){

            case 0:
                return listaActual.get(rowIndex).getNombre();
            case 1:
                return listaActual.get(rowIndex).getRegion();
            case 2:
                return listaActual.get(rowIndex).getComuna();
            case 3:
                return listaActual.get(rowIndex).getTipo();
            case 4:
                return listaActual.get(rowIndex).getComentario();
        }
        return "";
        
    }
    
}
